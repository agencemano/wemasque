module.exports = {
  prefix: 'tw-',
  purge: [
    'medik-child/resources/js/**/*.js',
    'medik-child/*.php',
    'medik-child/**/*.php',
  ],
  theme: {
    colors: {
      black: '#000000',
      'black-alt': '#19202a',
      white: '#ffffff',
      blue: '#0169e2',
      turquoise: '#1fbfd7',
      gray: '#f4f4f4',
      green: '#25c804',
      red: '#9f2124',
    },
    extend: {
      backgroundColor: {
        transparent: 'transparent'
      },
      maxWidth: {
        'xxs': '14rem',
        'xs': '16rem'
      },
      minWidth: {
        'plus-min-button': '135px'
      }
    },
  },
  variants: {},
  plugins: [],
}
