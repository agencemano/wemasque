<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;

$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );

if ( ! $short_description ) {
	return;
}

?>
<div class="woocommerce-product-details__short-description">
	<?php echo $short_description; // WPCS: XSS ok. ?>
</div>

<div class="tw-flex tw-items-center tw-my-8">
    <img class="tw-h-12 tw-mr-6"
         src="<?php echo get_stylesheet_directory_uri() . '/resources/images/picto_free-delivery_black.png'; ?>"
         alt="<?php _e( 'Camion de livraison gratuite', 'wemasque' ); ?>">
    <div class="tw-text-black tw-font-normal"><?php _e( 'Livraison gratuite sous 3 à 5 jours.', 'wemasque' ) ?></div>
</div>

