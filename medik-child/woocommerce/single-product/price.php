<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

$tax      = WC_Tax::get_rates( $product->get_tax_class() );
$tax_rate = reset( $tax )['rate'];

?>
<div class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>">

    <div class="woocommerce-Price-regular amount">
        <span><?php echo $product->get_price_html(); ?></span>
        <span class="tw-ml-1"><?php _e( 'HT', 'wemasque' ) ?></span>
    </div>

    <div class="woocommerce-Price-including-tax tw-mt-1">
		<?php
		echo sprintf(
			'(%s TTC - %s)',
			$product->is_on_sale() ? wc_price( wemasque_get_product_price_including_tax( $product->get_sale_price(), $tax_rate ) ) : wc_price( wemasque_get_product_price_including_tax( $product->get_price(), $tax_rate ) ),
			reset( $tax )['label']
		)
		?>
    </div>

</div>
