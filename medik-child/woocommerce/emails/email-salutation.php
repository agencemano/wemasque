<?php
defined( 'ABSPATH' ) || exit; ?>

<div id="salutation">
	<p><?php echo sprintf( esc_html__( "L'équipe Wemasque", 'alberine' )) ?></p>
	
	<a href="<?php echo home_url()?>"><?php echo home_url(); ?></a>
</div>
