<table class="document-footer" border="0" cellpadding="10" cellspacing="0" style="margin-top: 80px; !important">

    <tbody>

    <tr>

        <td align="center" valign="top">

            <p style="text-align: center !important">
				<?php _e(
					'Clauses de réserve de propriété : le vendeur conserve la propriété pleine et entière des marchandises vendues jusqu\'au paiement complet du prix, en application de la loi du 12 mai 1980. Tout différend quel qu\'il soit et quelles que soient les conditions de ventes, d\'exécution des commandes ou les modes de paiement acceptés est de la compétence excusive du Tribunal de Commerce du Siège Social de NGI. L\'article 3 de la loi 92/1442 du 31.12.1992 relative aux délais de paiement, prévoit que les pénalités doivent être calculées en cas de retard de paiement au taux d\'intérêt légal majoré de 1.5% au prorata du nombre de jours de retard par rapport à l\'échéance de la facture.',
					'wemasque' ) ?>
            </p>

            <p style="margin-top: 36px !important;">
				<?php _e( 'Wemasque est une marque déposée par la SAS NGI', 'wemasque' ) ?>
            </p>

        </td>

    </tr>

    </tbody>

</table>
