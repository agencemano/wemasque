<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
?>

<?php
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

$product_ids = wc_get_products( array(
	'order'  => 'desc',
	'return' => 'ids'
) )
?>

<?php if ( count( $product_ids ) ) : ?>

    <div id="homepage-carousel" class="swiper-container">
        
        <div class="swiper-wrapper">
			
			<?php foreach ( $product_ids as $product_id ) : ?>
				
				<?php
				$slider_data = get_field( 'slider_data', $product_id )
				?>

                <div class="swiper-slide tw-relative">
					
					<?php if ( $slider_data['slider_image_id'] ) : ?>
						
						<?php echo wp_get_attachment_image( $slider_data['slider_image_id'], 'full', FALSE ); ?>
					
					<?php endif; ?>
					
					<?php
					$text_position_class = $slider_data['is_text_on_left'] ? 'on-left' : 'on-right'
					?>

                    <div
                        class="<?php echo esc_attr( $text_position_class ); ?> product-info-wrapper tw-absolute tw-text-black tw-max-w-xxs tw-max-h-full md:tw-max-w-xs">

                        <h2 class="tw-font-bold tw-text-xl tw-text-black tw-leading-none lg:tw-text-3xl lg:tw-mb-4">
							<?php echo( $slider_data['slider_title'] ?: get_the_title( $product_id ) ) ?>
                        </h2>
						
						<?php if ( $slider_data['slider_text'] ) : ?>

                            <p class="tw-hidden tw-text-black tw-text-base tw-font-normal tw-leading-tight md:tw-block">
								<?php echo $slider_data['slider_text'] ?>
                            </p>
						
						<?php endif; ?>
						
						<?php if ( $slider_data['slider_button'] ) : ?>
							<?php
							$link_url    = $slider_data['slider_button']['url'];
							$link_title  = $slider_data['slider_button']['title'];
							$link_target = $slider_data['slider_button']['target'] ? $slider_data['slider_button']['target'] : '_self';
							?>

                            <a class="tw-btn tw-btn-turquoise tw-text-base"
                               href="<?php echo esc_url( $link_url ); ?>"
                               target="<?php echo esc_attr( $link_target ); ?>">
								
								<?php echo esc_html( $link_title ); ?>
                            </a>
						
						<?php endif; ?>

                    </div>

                </div>
			
			<?php endforeach; ?>
			
			<?php wp_reset_postdata(); ?>

        </div>

    </div>

    <div id="content" class="tw-padded-x">
		
		<?php if ( $first_block = get_field( 'first_block', get_queried_object()->ID ) ) : ?>
			<?php
			$first_block_related_product_id = $first_block['related_product_id']
			?>

            <div class="tw-w-full tw-flex tw-flex-col md:tw-flex-row">
                <div
                    class="tw-w-full tw-bg-blue tw-flex tw-justify-center tw-items-center md:tw-order-last md:tw-w-1/3 md:tw-ml-6">
                    <div class="tw-text-white tw-px-6 tw-py-4 tw-text-center tw-max-w-xs tw-mx-auto lg:tw-px-8">
                        <img class="tw-mx-auto"
                             src="<?php echo get_stylesheet_directory_uri() . '/resources/images/picto_badge-percent.png'; ?>"
                             alt="<?php _e( 'Badge pourcentage de réduction', 'wemasque' ); ?>">
                        <div class="tw-mt-8">
                            <div class="tw-text-2xl tw-uppercase tw-font-bold">
								<?php _e( 'Réalisez des économies' ) ?>
                            </div>
                            <div class="tw-text-xs tw-lowercase tw-font-normal">
								<?php _e( 'Grâce à nos tarifs dégressifs !' ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tw-w-full tw-mt-6 md:tw-order-first md:tw-w-2/3 md:tw-mt-0">
                    <div
                        class="tw-h-full tw-w-full tw-flex tw-flex-col tw-px-4 tw-pt-6 tw-bg-gray md:tw-flex-row md:tw-pr-0 md:tw-pb-6 lg:tw-px-8">
                        <div class="md:tw-w-3/5">
                            <h2 class="tw-font-bold tw-text-xl tw-text-black lg:tw-text-3xl"><?php echo( $first_block['title'] ?: get_the_title( $first_block_related_product_id ) ) ?></h2>
                            <p class="tw-leading-tight"><?php echo $first_block['text']; ?></p>
                            <a class="tw-btn tw-btn-blue tw-text-base"
                               href="<?php echo get_permalink( $first_block_related_product_id ) ?>">
								<?php echo $first_block['button_label']; ?>
                            </a>
                        </div>
                        <div
                            class="tw-mt-6 md:tw-mt-0 md:tw-w-auto md:tw-w-2/5 md:tw-ml-3 md:tw-flex md:tw-justify-center md:tw-items-center">
							
							<?php if ( $homepage_image_id = get_field( 'homepage_image', $first_block_related_product_id ) ) : ?>
								
								<?php echo wp_get_attachment_image( $homepage_image_id, 'full', FALSE ); ?>
							
							<?php else: ?>
								
								<?php echo wp_get_attachment_image( get_post_thumbnail_id( $first_block_related_product_id ), 'full', FALSE ); ?>
							
							<?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
		
		<?php endif; ?>
		
		<?php if ( $second_block = get_field( 'second_block', get_queried_object()->ID ) ) : ?>
			<?php
			$second_block_related_product_id = $second_block['related_product_id']
			?>

            <div class="tw-w-full tw-flex tw-flex-col tw-mt-6 md:tw-flex-row">
                <div class="tw-w-full tw-bg-turquoise tw-flex tw-justify-center tw-items-center md:tw-w-1/3 md:tw-mr-6">
                    <div class="tw-text-white tw-px-6 tw-py-4 tw-text-center tw-max-w-xs tw-mx-auto lg:tw-px-8">
                        <img class="tw-mx-auto"
                             src="<?php echo get_stylesheet_directory_uri() . '/resources/images/picto_free-delivery.png'; ?>"
                             alt="<?php _e( 'Camion de livraison gratuite', 'wemasque' ); ?>">
                        <div class="tw-mt-8">
                            <div class="tw-text-2xl tw-uppercase tw-font-bold">
								<?php _e( 'Livraison gratuite' ) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tw-w-full tw-mt-6 md:tw-w-2/3 md:tw-mt-0">
                    <div
                        class="tw-h-full tw-w-full tw-flex tw-flex-col tw-px-4 tw-pt-6 tw-bg-gray md:tw-flex-row md:tw-pr-0 md:tw-pb-6 lg:tw-px-8">
                        <div class="md:tw-w-3/5">
                            <h2 class="tw-font-bold tw-text-xl tw-text-black lg:tw-text-3xl"><?php echo( $second_block['title'] ?: get_the_title( $second_block_related_product_id ) ) ?></h2>
                            <p class="tw-leading-tight"><?php echo $second_block['text']; ?></p>
                            <a class="tw-btn tw-btn-blue tw-text-base"
                               href="<?php echo get_permalink( $second_block_related_product_id ) ?>">
								<?php echo $second_block['button_label']; ?>
                            </a>
                        </div>
                        <div
                            class="tw-mt-6 md:tw-mt-0 md:tw-w-auto md:tw-w-2/5 md:tw-ml-3 md:tw-flex md:tw-justify-center md:tw-items-center">
							
							<?php if ( $homepage_image_id = get_field( 'homepage_image', $second_block_related_product_id ) ) : ?>
								
								<?php echo wp_get_attachment_image( $homepage_image_id, 'full', FALSE ); ?>
							
							<?php else: ?>
								
								<?php echo wp_get_attachment_image( get_post_thumbnail_id( $second_block_related_product_id ), 'full', FALSE ); ?>
							
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
		
		<?php endif; ?>
		
		<?php if ( $third_block = get_field( 'third_block', get_queried_object()->ID ) ) : ?>
			<?php
			$third_block_related_product_id = $third_block['related_product_id']
			?>

            <div class="tw-w-full tw-flex tw-flex-col tw-mt-6 md:tw-flex-row">
                <div
                    class="tw-w-full tw-bg-blue tw-flex tw-justify-center tw-items-center md:tw-order-last md:tw-w-1/3 md:tw-ml-6">
                    <div class="tw-text-white tw-px-6 tw-py-4 tw-text-center tw-max-w-xs tw-mx-auto lg:tw-px-8">
	                    <img class="tw-mx-auto"
	                         src="<?php echo get_stylesheet_directory_uri() . '/resources/images/picto_recyclage.png'; ?>"
	                         alt="<?php _e( 'Nous recyclons vos masques', 'wemasque' ); ?>">
                        <div class="tw-mt-8 tw-text-2xl tw-uppercase tw-font-bold md:tw-text-xl"><!--
							--><?php _e( 'Nous recyclons vos masques' ) ?><!--
                        --></div>
                    </div>
                </div>
                <div class="tw-w-full tw-mt-6 md:tw-order-first md:tw-w-2/3 md:tw-mt-0">
                    <div
                        class="tw-h-full tw-w-full tw-flex tw-flex-col tw-px-4 tw-pt-6 tw-bg-gray md:tw-flex-row md:tw-pr-0 md:tw-pb-6 lg:tw-px-8">
                        <div class="md:tw-w-3/5">
                            <h2 class="tw-font-bold tw-text-xl tw-text-black lg:tw-text-3xl"><?php echo( $third_block['title'] ?: get_the_title( $third_block_related_product_id ) ) ?></h2>
                            <p class="tw-leading-tight"><?php echo $third_block['text']; ?></p>
                            <a class="tw-btn tw-btn-blue tw-text-base"
                               href="<?php echo get_permalink( $third_block_related_product_id ) ?>">
								<?php echo $third_block['button_label']; ?>
                            </a>
                        </div>
                        <div
                            class="tw-mt-6 md:tw-mt-0 md:tw-w-auto md:tw-w-2/5 md:tw-ml-3 md:tw-flex md:tw-justify-center md:tw-items-center">
							
							<?php if ( $homepage_image_id = get_field( 'homepage_image', $third_block_related_product_id ) ) : ?>
								
								<?php echo wp_get_attachment_image( $homepage_image_id, 'full', FALSE ); ?>
							
							<?php else: ?>
								
								<?php echo wp_get_attachment_image( get_post_thumbnail_id( $third_block_related_product_id ), 'full', FALSE ); ?>
							
							<?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
		
		<?php endif; ?>

    </div>

<?php else: ?>
	
	<?php
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
	?>

<?php endif; ?>

<?php
/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
?>

<?php
get_footer( 'shop' );

