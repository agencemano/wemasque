<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta class="netreviewsWidget" id="netreviewsWidgetNum23326"
          data-jsurl="//cl.avis-verifies.com/fr/cache/c/5/5/c55f92e2-1dd8-4944-11fd-5527deb1ee69/widget4/widget18-23326_script.js"/>
    <script src="//cl.avis-verifies.com/fr/widget4/widget18_FB3.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47097720-3"></script>
    <script>
      window.dataLayer = window.dataLayer || []

      function gtag () {dataLayer.push(arguments)}

      gtag('js', new Date())
      gtag('config', 'UA-47097720-3')
    </script>
    
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Structured Data Schema -->
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Wemasque",
        "url": "<?php echo home_url(); ?>",
        "address": "ZA Charles Granger 72600 MAMERS",
        "sameAs": [
            "https://www.facebook.com/wemasquefr/",
            "https://www.linkedin.com/company/wemasquefr/"
        ]
    }
</script>

<?php wp_body_open(); ?>
<?php

/**
 * medik_hook_top hook.
 *      medik_hook_top - 10
 *      medik_page_loader - 20
 */
do_action( 'medik_hook_top' );
?>

<!-- **Wrapper** -->
<div class="wrapper">
    
    <?php global $wp_query; ?>
    <h1 class="tw-sr-only"><?php echo $wp_query->post->post_title ?></h1>

    <!-- ** Inner Wrapper ** -->
    <div class="inner-wrapper">
		
		<?php
		/**
		 * medik_hook_content_before hook.
		 *
		 */
		do_action( 'medik_hook_content_before' );
		?>
