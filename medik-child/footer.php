<?php
/**
 * medik_hook_content_after hook.
 *
 */
do_action( 'medik_hook_content_after' );
?>

<?php get_template_part( 'template-parts/section-guarantee' ); ?>

<!-- **Footer** -->
<footer id="footer">
    <div class="tw-py-4 tw-bg-black-alt lg:tw-pt-8">

        <div class="tw-w-full tw-padded-x tw-w-full tw-pb-4 lg:tw-flex lg:tw-flex-row">
            <div class="tw-w-full lg:tw-w-3/5 lg:tw-pr-4 xl:tw-w-2/3 xl:tw-pr-10">
                <p class="tw-font-bold"><?php _e( 'A propos de Wemasque', 'wemasque' ) ?></p>
                <p><?php _e( 'Wemasque est une marque de la société NGI.', 'wemasque' ) ?></p>
                <p class="tw-leading-tight">
                    <a href="https://ngi.fr/" class="tw-block tw-w-full tw-h-full no-hover-effect">
						<?php _e( "NGI est une société sarthoise (72) spécialisée dans la conception, la fabrication de produits multimatériaux et la distribution. Avec ses 35 années d'expérience, et sa structure en France et en Chine, elle produit plus de 1500 références destinées aux particuliers et professionnels.", 'wemasque' ) ?>
                    </a>
                </p>
            </div>
            <div class="tw-w-full lg:tw-w-2/5 xl:tw-w-1/3">
                <div class="tw-inline-flex tw-items-center">
                    <span
                        class="tw-inline-block tw-font-bold"><?php _e( 'Suivez-nous sur les réseaux :', 'wemasque' ) ?></span>
                    <div class="tw-inline-flex tw-items-center tw-ml-2">
                        <a href="https://www.facebook.com/wemasquefr/" target="_blank" class="tw-text-white tw-inline-block">
                            <svg class="tw-fill-current tw-h-12 xl:tw-h-8" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 512 512">
                                <path
                                    d="M448 0H64C28.704 0 0 28.704 0 64v384c0 35.296 28.704 64 64 64h192V336h-64v-80h64v-64c0-53.024 42.976-96 96-96h64v80h-32c-17.664 0-32-1.664-32 16v64h80l-32 80h-48v176h96c35.296 0 64-28.704 64-64V64c0-35.296-28.704-64-64-64z"/>
                            </svg>
                        </a>
                        <a href="https://www.linkedin.com/company/wemasquefr/" target="_blank" class="tw-text-white tw-inline-block tw-pl-2">
                            <svg class="tw-fill-current tw-h-12 xl:tw-h-8" viewBox="0 0 512 512"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M475.074 0H36.926C16.53 0 0 16.531 0 36.926v438.148C0 495.47 16.531 512 36.926 512h438.148C495.47 512 512 495.469 512 475.074V36.926C512 16.53 495.469 0 475.074 0zM181.61 387h-62.347V199.426h62.347zm-31.172-213.188h-.406c-20.922 0-34.453-14.402-34.453-32.402 0-18.406 13.945-32.41 35.274-32.41 21.328 0 34.453 14.004 34.859 32.41 0 18-13.531 32.403-35.274 32.403zM406.423 387h-62.34V286.652c0-25.218-9.027-42.418-31.586-42.418-17.223 0-27.48 11.602-31.988 22.801-1.649 4.008-2.051 9.61-2.051 15.215V387h-62.344s.817-169.977 0-187.574h62.344v26.558c8.285-12.78 23.11-30.96 56.188-30.96 41.02 0 71.777 26.808 71.777 84.421zm0 0"/>
                            </svg>
                        </a>
                    </div>
                </div>
                <p class="tw-mt-4"><?php _e( "NGI - Wemasque <br> ZA Charles Granger <br> 72 600 MAMERS <br> 02.43.33.56.86 <br> info@wemasque.fr", "wemasque" ) ?></p>
            </div>
        </div>

        <div
            class="tw-w-full tw-pt-4 tw-padded-x tw-border-t tw-border-solid tw-border-black tw-border-opacity-50">
			<?php
			if ( has_nav_menu( 'menu-footer' ) ) {
				wp_nav_menu( array(
					'theme_location' => 'menu-footer',
					'menu_id'        => 'footer-menu',
					'menu_class'     => 'menu-footer',
					'container'      => 'nav',
				) );
			}
			?>
        </div>

    </div>
</footer><!-- **Footer - End** -->

</div><!-- **Inner Wrapper - End** -->

</div><!-- **Wrapper - End** -->
<?php

do_action( 'medik_hook_bottom' );

wp_footer();
?>
</body>
</html>
