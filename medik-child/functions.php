<?php

function medik_child_enqueue_styles() {
	
	wp_enqueue_style( 'medik-parent', get_theme_file_uri( '/style.css' ) );
	
	wp_enqueue_style( 'medik-child-style', get_stylesheet_directory_uri() . '/style.css', array() );
	
	wp_enqueue_style( 'swiper-slider-css', 'https://unpkg.com/swiper/swiper-bundle.min.css', FALSE );
	
	wp_enqueue_script( 'medik-child-script', get_stylesheet_directory_uri() . '/app.js', array(
		'jquery',
		'underscore'
	), '0.1', TRUE );
	
	if ( is_product() ) {
		wp_enqueue_script( 'medik-child-single-product-script', get_stylesheet_directory_uri() . '/public/js/single-product.js', array(
			'jquery',
			'underscore'
		), '0.1', TRUE );
	}
	
	if ( is_cart() ) {
		wp_enqueue_script( 'medik-child-cart-script', get_stylesheet_directory_uri() . '/public/js/cart.js', array(
			'jquery',
		), '0.1', TRUE );
	}
}

add_action( 'wp_enqueue_scripts', 'medik_child_enqueue_styles', 100 );


if ( ! function_exists( 'wemasque_setup' ) ) :
	
	function wemasque_setup() {
		add_theme_support( 'woocommerce' );
		
		register_nav_menus(
			array(
				'menu-footer' => __( 'Menu bas de page', 'wemasque' ),
			)
		);
		
		add_theme_support( 'custom-logo' );
		
		wemasque_includes();
	}
endif;
add_action( 'after_setup_theme', 'wemasque_setup' );


/*
* Additional includes.
*/
function wemasque_includes() {
	require get_stylesheet_directory() . '/inc/extras.php';
	
	require get_stylesheet_directory() . '/inc/woocommerce.php';
}



