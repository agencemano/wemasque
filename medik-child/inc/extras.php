<?php

if ( ! function_exists( 'woocommerce_template_single_stock' ) ) {
	
	/**
	 * Output the product stock.
	 */
	function woocommerce_template_single_stock() {
		wc_get_template( 'single-product/stock.php' );
	}
}

if ( ! function_exists( 'woocommerce_output_product_description' ) ) {
	
	/**
	 * Output the product description.
	 */
	function woocommerce_output_product_description() {
		get_template_part( 'template-parts/single-product/description' );
	}
}

if ( ! function_exists( 'woocommerce_output_product_technical_details' ) ) {
	
	/**
	 * Output the product technical details.
	 */
	function woocommerce_output_product_technical_details() {
		get_template_part( 'template-parts/single-product/technical-details' );
	}
}

if ( ! function_exists( 'woocommerce_output_product_decreasing_prices' ) ) {
	
	/**
	 * Output the product decreasing prices table.
	 */
	function woocommerce_output_product_decreasing_prices() {
		get_template_part( 'template-parts/single-product/decreasing-prices' );
	}
}

if ( ! function_exists( 'wemasque_woo_output_content_wrapper' ) ) {
	
	function wemasque_woo_output_content_wrapper() {
		
		$shop_page_id      = '';
		$settings          = array();
		$page_layout       = '';
		$sidebar_class     = '';
		$show_sidebar      = '';
		$show_left_sidebar = '';
		$container_class   = '';
		$widgets           = array();
		
		if ( is_shop() || is_product_category() || is_product_tag() ) {
			
			$options = medik_shop_archive_page();
			
			$widgets = $options['widgets'];
			$layout  = $options['page-layout'];
			
			$layout = medik_page_layout( $layout );
			extract( $layout );
			
		} elseif ( is_product() ) {
			
			global $post;
			
			$options = medik_single_product( $post->ID, "left" );
			
			$widgets = $options['widgets'];
			$layout  = $options['page-layout'];
			
			$layout = medik_page_layout( $layout );
			extract( $layout );
			
		}
		
		$header_class = cs_get_option( 'breadcrumb-position' );
		
		echo '<!-- ** Header Wrapper ** -->';
		echo '<div id="header-wrapper" class="' . esc_attr( $header_class ) . '">';
		
		echo '<!-- **Header** -->';
		echo '<header id="header">';
		
		echo '<div class="container">';
		do_action( 'medik_header', $shop_page_id );
		echo '</div>';
		
		echo '</header>';
		echo '<!-- **Header - End ** -->';
		
		# Shop
		if ( is_shop() ) {
			
			$shop_page_enable_breadcrumb = cs_get_option( 'shop-page-enable-breadcrumb' );
			$shop_page_enable_breadcrumb = ( isset( $shop_page_enable_breadcrumb ) && ! empty( $shop_page_enable_breadcrumb ) ) ? TRUE : FALSE;
			
			if ( $shop_page_enable_breadcrumb ) {
				
				if ( get_option( 'woocommerce_shop_page_id' ) == '' ) {
					$breadcrumbs[] = '<span class="current">' . esc_html__( 'Shop', 'medik' ) . '</span>';
					$bstyle        = medik_cs_get_option( 'breadcrumb-style', 'default' );
					$style         = medik_breadcrumb_css();
					
					medik_breadcrumb_output( '<h1>' . esc_html__( 'Shop', 'medik' ) . '</h1>', $breadcrumbs, $bstyle, $style );
				} else {
					$breadcrumbs[] = '<span class="current">' . get_the_title( get_option( 'woocommerce_shop_page_id' ) ) . '</span>';
					$bstyle        = medik_cs_get_option( 'breadcrumb-style', 'default' );
					$style         = medik_breadcrumb_css();
					
					medik_breadcrumb_output( '<h1>' . get_the_title( get_option( 'woocommerce_shop_page_id' ) ) . '</h1>', $breadcrumbs, $bstyle, $style );
				}
			}
		}
		
		# Product
		if ( is_product() ) {
			
			$dt_single_product_enable_breadcrumb = cs_get_option( 'dt-single-product-enable-breadcrumb' );
			$dt_single_product_enable_breadcrumb = ( isset( $dt_single_product_enable_breadcrumb ) && ! empty( $dt_single_product_enable_breadcrumb ) ) ? TRUE : FALSE;
			
			if ( $dt_single_product_enable_breadcrumb ) {
				
				global $post;
				
				$breadcrumbs[] = the_title( '<span class="current">', '</span>', FALSE );
				$bstyle        = medik_cs_get_option( 'breadcrumb-style', 'default' );
				$style         = medik_breadcrumb_css();
				
				medik_breadcrumb_output( '<h1>' . get_the_title( $post->ID ) . '</h1>', $breadcrumbs, $bstyle, $style );
			}
		}
		
		# Product Category
		if ( is_product_category() ) {
			
			$dt_woo_category_archive_enable_breadcrumb = cs_get_option( 'dt-woo-category-archive-enable-breadcrumb' );
			$dt_woo_category_archive_enable_breadcrumb = ( isset( $dt_woo_category_archive_enable_breadcrumb ) && ! empty( $dt_woo_category_archive_enable_breadcrumb ) ) ? TRUE : FALSE;
			
			if ( $dt_woo_category_archive_enable_breadcrumb ) {
				
				$breadcrumbs[] = '<a href="' . get_the_permalink( get_option( 'woocommerce_shop_page_id' ) ) . '">' . get_the_title( get_option( 'woocommerce_shop_page_id' ) ) . '</a>';
				$breadcrumbs[] = '<span class="current">' . single_term_title( '', FALSE ) . '</span>';
				$bstyle        = medik_cs_get_option( 'breadcrumb-style', 'default' );
				$style         = medik_breadcrumb_css();
				
				medik_breadcrumb_output( '<h1>' . single_term_title( '', FALSE ) . '</h1>', $breadcrumbs, $bstyle, $style );
				
			}
			
		}
		
		# Product Tag
		if ( is_product_tag() ) {
			
			$dt_woo_tag_archive_enable_breadcrumb = cs_get_option( 'dt-woo-tag-archive-enable-breadcrumb' );
			$dt_woo_tag_archive_enable_breadcrumb = ( isset( $dt_woo_tag_archive_enable_breadcrumb ) && ! empty( $dt_woo_tag_archive_enable_breadcrumb ) ) ? TRUE : FALSE;
			
			if ( $dt_woo_tag_archive_enable_breadcrumb ) {
				
				$breadcrumbs[] = '<a href="' . get_the_permalink( get_option( 'woocommerce_shop_page_id' ) ) . '">' . get_the_title( get_option( 'woocommerce_shop_page_id' ) ) . '</a>';
				$breadcrumbs[] = '<span class="current">' . single_term_title( '', FALSE ) . '</span>';
				$bstyle        = medik_cs_get_option( 'breadcrumb-style', 'default' );
				$style         = medik_breadcrumb_css();
				
				medik_breadcrumb_output( '<h1>' . single_term_title( '', FALSE ) . '</h1>', $breadcrumbs, $bstyle, $style );
				
			}
			
		}
		
		
		echo '</div>';
		echo '<!-- ** Header Wrapper - End ** -->';
		
		echo '<!-- **Main** -->';
		echo '<div id="main">';
		
		echo '<!-- **Main - Container** -->';
		echo '<div class="container">';
		
		# Left Sidebar
		/*if ( $show_sidebar ) {
			if ( $show_left_sidebar ) {
				$wtstyle = cs_get_option( 'wtitle-style' );

				echo '<section id="secondary-left" class="secondary-sidebar '.esc_attr( $sidebar_class ).'">';
					echo !empty( $wtstyle ) ? "<div class='{$wtstyle}'>" : '';
					foreach( $widgets as $widget ) {
						dynamic_sidebar( $widget );
					}
					echo !empty( $wtstyle ) ? '</div>' : '';
				echo '</section>';
			}
		}*/
		# Left Sidebar
		
		
		echo '<!-- Primary -->';
		echo '<section id="primary" class="' . esc_attr( $page_layout ) . '">';
		
	}
}

if ( ! function_exists( 'wemasque_get_product_price_including_tax' ) ) {
	
	/**
	 * Output the price including the tax rate.
	 */
	function wemasque_get_product_price_including_tax( $price, $tax_rate, $is_the_tax_rate_in_percentage = TRUE ) {
		
		if ( $is_the_tax_rate_in_percentage ) {
			$tax_rate = $tax_rate / 100;
		}
		
		return $price + ( $price * $tax_rate );
	}
}

if ( ! function_exists( 'wemasque_woo_loop_product_button_elements_cart' ) ) {
	
	function wemasque_woo_loop_product_button_elements_cart() {
		
		global $product;
		$product_id = $product->get_id();
		
		echo '<div class="wcqv_btn_wrapper wc_btn_inline" data-tooltip="' . esc_attr__( "Voir le produit', 'wemasque" ) . '"><a class="button yith-wcqv-button" href="' . get_permalink( $product_id ) . '">' . esc_attr__( 'Voir le produit', 'wemasque' ) . '</a></div>';
	}
}

if ( ! function_exists( 'wemasque_custom_kiwiz_example_footer' ) ) {
	
	function wemasque_custom_kiwiz_example_footer() {
		
		wc_get_template( 'kiwiz/example/footer.php' );
	}
}

if ( ! function_exists( 'wemasque_custom_kiwiz_invoice_footer' ) ) {
	
	function wemasque_custom_kiwiz_invoice_footer() {
		
		wc_get_template( 'kiwiz/invoice/footer.php' );
	}
}

// Override DesignThemes Core Features sharing function
if(!function_exists('wemasque_blog_single_social_share')) {
	function wemasque_blog_single_social_share($post_ID) {
		
		$output = '<div class="share">';
		
		$title = get_the_title( $post_ID );
		$title = urlencode($title);
		
		$link = get_permalink( $post_ID );
		$link = rawurlencode( $link );
		
		$output .= '<i class="fas fa-share-alt-square"></i>';
		$output .= '<ul class="dt-share-list">';
		$output .= '<li><a href="http://www.facebook.com/sharer.php?u='.esc_attr($link).'&amp;t='.esc_attr($title).'" class="fab fa-facebook-f" target="_blank"></a></li>';
		$output .= '<li><a href="http://twitter.com/share?text='.esc_attr($title).'&amp;url='.esc_attr($link).'" class="fab fa-twitter" target="_blank"></a></li>';
		$output .= '<li><a href="https://www.linkedin.com/sharing/share-offsite/?url='.esc_attr($link).'" class="fab fa-linkedin" target="_blank"></a></li>';
		$output .= '<li><a href="http://pinterest.com/pin/create/button/?url='.esc_attr($link).'&media='.get_the_post_thumbnail_url($post_ID, 'full').'" class="fab fa-pinterest" target="_blank"></a></li>';
		$output .= '</ul>';
		
		$output .= '</div>';
		
		return $output;
		
	}
}