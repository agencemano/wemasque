<?php

/**
 * Product page tabs.
 */
remove_filter( 'woocommerce_product_tabs', 'woocommerce_default_product_tabs' );
remove_filter( 'woocommerce_product_tabs', 'woocommerce_sort_product_tabs', 99 );

/**
 * Product Summary Box.
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_stock', 15 );

/**
 * After Single Products Summary Div.
 */
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

//add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_decreasing_prices', 35 );

add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_description', 5 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_technical_details', 10 );

/*
 * Breadcrumb.
 */
remove_action( 'woocommerce_before_main_content', 'medik_woo_output_content_wrapper', 11 );

add_action( 'woocommerce_before_main_content', 'wemasque_woo_output_content_wrapper', 11 );

/*
 * Woocommerce gateway Stripe.
 */
function wemasque_custom_stripe_payment_icons( $icons ) {
	
	$payment_icons_src = home_url() . '/wp-content/plugins/woocommerce/assets/images/icons/credit-cards';
	
	unset( $icons['amex'] );
	
	$icons['mastercard'] = "<img class='stripe-icon' src='{$payment_icons_src}/mastercard.svg' />";
	$icons['visa']       = "<img class='stripe-icon' src='{$payment_icons_src}/visa.svg' />";
	
	return $icons;
}

add_filter( 'wc_stripe_payment_icons', 'wemasque_custom_stripe_payment_icons' );


/*
 * My account orders columns.
 */
function wemasque_custom_account_orders_columns( $columns ) {
	
	if ( $columns['order-total'] ) {
		$columns['order-total'] = __( 'Total (TTC)', 'wemasque' );
	}
	
	return $columns;
}

add_filter( 'woocommerce_account_orders_columns', 'wemasque_custom_account_orders_columns' );

/*
 * Related product item button action.
 */
remove_action( 'dt_woo_loop_product_button_elements_cart', 'medik_woo_loop_product_button_elements_cart' );

add_action( 'dt_woo_loop_product_button_elements_cart', 'wemasque_woo_loop_product_button_elements_cart' );

/*
 * Kiwiz custom footer.
 */
add_action( 'kiwiz_document_example_items', 'wemasque_custom_kiwiz_example_footer', 20 );
add_action( 'kiwiz_document_invoice_items', 'wemasque_custom_kiwiz_invoice_footer', 20 );

/*
 * Emails.
 */
add_action( 'wemasque_after_email_paragraph', 'wemasque_email_salutation', 10, 3 );

function wemasque_email_salutation( $sent_to_admin = FALSE, $plain_text = FALSE, $email = '' ) {
	if ( $plain_text ) {
		wc_get_template(
			'emails/plain/email-salutation.php',
			array(
				'sent_to_admin' => $sent_to_admin,
				'plain_text'    => $plain_text,
				'email'         => $email,
			)
		);
	} else {
		wc_get_template(
			'emails/email-salutation.php',
			array(
				'sent_to_admin' => $sent_to_admin,
				'plain_text'    => $plain_text,
				'email'         => $email,
			)
		);
	}
}

/*
 * Shipping address.
 */
add_filter( 'woocommerce_shipping_fields', 'wemasque_add_field' );
function wemasque_add_field( $fields ) {
	
	$fields['shipping_phone'] = array(
		'label'    => __( 'Téléphone', 'wemasque' ),
		'required' => TRUE,
		'clear'    => TRUE,
		'validate' => array( 'phone' ),
		'class'    => array( 'form-row-wide' ),
	);
	
	return $fields;
	
}

add_filter( 'woocommerce_customer_meta_fields', 'wemasque_admin_address_field' );
function wemasque_admin_address_field( $admin_fields ) {
	
	$admin_fields['shipping']['fields']['billing_shipping_phone'] = array(
		'label'       => __( 'Téléphone', 'wemasque' ),
		'description' => '',
	);
	
	return $admin_fields;
	
}

add_filter( 'woocommerce_my_account_my_address_formatted_address', 'wemasque_custom_my_account_my_address_formatted_address', 10, 3 );
function wemasque_custom_my_account_my_address_formatted_address( $fields, $customer_id, $type ) {
	
	if ( $type == 'shipping' ) {
		
		$fields['shipping_phone'] = get_user_meta( $customer_id, 'shipping_phone', TRUE );
	}
	
	return $fields;
}

add_filter( 'woocommerce_order_get_formatted_shipping_address', 'wemasque_custom_order_get_formatted_shipping_address', 10, 3 );
function wemasque_custom_order_get_formatted_shipping_address( $address, $raw_address, $order ) {
	
	$customer_id = $order->get_customer_id();
	
	$address .= '<br>' . get_user_meta( $customer_id, 'shipping_phone', TRUE );
	
	return $address;
}

add_action( 'woocommerce_checkout_update_order_meta', 'wemasque_custom_checkout_field_update_order_meta' );
function wemasque_custom_checkout_field_update_order_meta( $order_id ) {
	if ( ! empty( $_POST['shipping_phone'] ) ) {
		update_post_meta( $order_id, 'shipping_phone', sanitize_text_field( $_POST['shipping_phone'] ) );
	}
}


