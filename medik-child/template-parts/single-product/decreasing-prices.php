<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

<section id="decreasing_prices" class="tw-mt-6">
    <h2 class="tw-text-2xl tw-font-bold tw-text-black tw-mb-8">
		<?php _e( 'Tarifs dégressifs', 'wemasque' ) ?>
    </h2>
	<?php echo wp_get_attachment_image( get_field( 'decreasing_prices', $product->get_id() ), 'full', FALSE ); ?>
</section>
