<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

?>

<section id="description" class="tw-mt-12">
    <h2 class="tw-text-2xl tw-font-bold tw-text-black tw-mb-8">
		<?php _e( 'Descriptif produit', 'wemasque' ) ?>
    </h2>
	<p class="tw-text-black"><?php echo get_the_content( NULL, FALSE, $post->ID ); ?></p>
</section>
