<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

<section id="technical_details" class="tw-mt-12">
    <h2 class="tw-text-2xl tw-font-bold tw-text-black tw-mb-8">
		<?php _e( 'Details techniques', 'wemasque' ) ?>
    </h2>
    <div class="content">
	    <?php the_field( 'technical_details', $product->get_id() ); ?>
    </div>
</section>
