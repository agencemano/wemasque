import Swiper from 'swiper'

jQuery(document).ready(function ($) {
  var slider = new Swiper('#homepage-carousel', {
    loop: true,
    autoplay: {
      delay: 5000,
    },
  })
})
