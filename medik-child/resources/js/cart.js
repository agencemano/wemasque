jQuery(document).ready(function ($) {
  // Removes Woocommerce Ajax behaviours.
  // disable_ajax_on_cart_product_remove()

  // Trigger a fake click on the update_cart button.
  dirty_trigger_cart_update()

  /*
  * Helpers
  */
  var is_blocked = function ($node) {
    return $node.hasClass('processing')
  }

  var block = function ($node, invisible) {
    if (!is_blocked($node))
    {
      var overlay

      if (invisible)
      {
        overlay = {
          background: '#fff',
          opacity: 0,
        }
      }
      else
      {
        overlay = {
          background: '#fff',
          opacity: 0.6,
        }
      }

      $node.addClass('processing').block({
        message: null,
        overlayCSS: overlay,
      })
    }
  }

  var unblock = function ($node) {
    $node.removeClass('processing').unblock()
  }

  function disable_ajax_on_cart_product_remove ()
  {
    $(document).off(
      'click',
      '.woocommerce-cart-form .product-remove > a',
    )
    $(document).on(
      'click',
      '.woocommerce-cart-form .product-remove > a', function (event) {
        event.preventDefault()

        var $a = $(event.currentTarget)
        var $form = $a.parents('form')

        block($form, false)
        block($('div.cart_totals'), false)

        location.replace($a.attr('href'))
      },
    )
  }

  function dirty_trigger_cart_update ()
  {
    setTimeout(function () {
      var $form = $('.woocommerce-cart-form')
      var $update_cart_button = $form.find(':input[name="update_cart"]')

      if ($update_cart_button.length > 0)
      {
        block($form, true)

        $update_cart_button.attr('clicked', 'true')
        $form.submit()

        unblock($form)
      }
    }, 25)
  }
});
