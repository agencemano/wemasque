jQuery(document).ready(function ($) {

  var $qty_input = $('.product form.cart').find('input.qty').first()
  var $helper_text = $('#items-bundle-count')
  var $total_items_text = $helper_text.find('span.total')
  var total_items_per_bundle = $helper_text.data('items-per-bundle')

  function setTotalItemsText (value)
  {
    $total_items_text.text(parseInt(total_items_per_bundle) * value)
  }

  $qty_input.on('change', function (event) {

    setTotalItemsText($(this).val())

  })

  setTotalItemsText($qty_input.val())
})
