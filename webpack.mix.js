const mix = require('laravel-mix')

mix.postCss('medik-child/resources/css/main.css',
  'medik-child/style.css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('postcss-nested'),
    require('autoprefixer'),
  ]).js(
  'medik-child/resources/js/index.js',
  'medik-child/app.js',
).js(
  'medik-child/resources/js/single-product.js',
  'medik-child/public/js/single-product.js',
).js(
  'medik-child/resources/js/cart.js',
  'medik-child/public/js/cart.js',
).options({
  processCssUrls: false,
})
